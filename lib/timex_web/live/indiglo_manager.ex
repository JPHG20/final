defmodule TimexWeb.IndigloManager do

  use GenServer
  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, st: IndigloOff, count: 0}}
  end

  def handle_info(:"top-right", %{st: IndigloOff, ui_pid: ui} = state) do
    GenServer.cast(ui, :set_indiglo)

    {:noreply, %{state | st: IndigloOn, count: 1}}
  end

  def handle_info(:"top-right", %{ui_pid: ui, st: IndigloOn} = state) do
    GenServer.cast(ui, :unset_indiglo)

    Process.send_after(self(), Waiting2IndigloOff, 2000)
    {:noreply, %{state | st: Waiting, count: 0} }
  end

  def handle_info(Waiting2IndigloOff, %{st: Waiting, ui_pid: ui} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, %{state | st: IndigloOff, count: 0}}
  end

  def handle_info(:alarmOff, %{ui_pid: ui} = state) do
    GenServer.cast(ui, :unset_indiglo)

    Process.send_after(self(), :alarm, 1000)
    {:noreply, state}
  end

  def handle_info(:alarm, %{ui_pid: ui, st: IndigloOn, count: count} = state) do
    if(count <= 9) do
      count=count+1
      GenServer.cast(ui, :set_indiglo)
      Process.send_after(self(), :alarmOff, 1000)
      {:noreply, %{state | count: count}}
    else
      {:noreply, %{state | st: IndigloOff, count: 9}}
    end
  end

  def handle_info(state) do
    {:noreply, state}
  end
end
